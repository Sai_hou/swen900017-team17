package com.service;

import java.util.List;

import com.domain.Houseinformation;

public interface HouseService {
	
	public void addHouse(Houseinformation house);
	public void deleteHouse(Houseinformation house);
	public List<Houseinformation> allHouse();
	public void updateHouse(Houseinformation house);
	public Houseinformation findHouseById(int id);
}
