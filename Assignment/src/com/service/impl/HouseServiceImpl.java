package com.service.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;

import com.domain.Houseinformation;
import com.service.HouseService;

public class HouseServiceImpl implements HouseService {

	private SessionFactory sessionFactory;

	@Override
	public void addHouse(Houseinformation house) {
		// TODO Auto-generated method stub
		// Session
		Session se = sessionFactory.openSession();
		Transaction tx = se.beginTransaction();
		// save
		se.save(house);
		// transcation commit
		tx.commit();
		se.close();
	}

	public void deleteHouse(Houseinformation house) {
		Session se = sessionFactory.openSession();
		Transaction tx = se.beginTransaction();
		se.delete(house);
		tx.commit();
	}

	public List<Houseinformation> allHouse() {
		
		Session se = sessionFactory.openSession();
		Transaction tx = se.beginTransaction();
		Criteria criteria =se.createCriteria(Houseinformation.class);
		List<Houseinformation> houseList = criteria.list();
		System.out.println(houseList.size());
		tx.commit();
		return houseList;
	}
	
	@Override
	public Houseinformation findHouseById(int id) {
		Session se = sessionFactory.openSession();
		Transaction tx = se.beginTransaction();
		Houseinformation house = (Houseinformation)se.load(Houseinformation.class, id);
		return house;
	}

	public void updateHouse(Houseinformation house) {
		Session se = sessionFactory.openSession();
		Transaction tx = se.beginTransaction();
		se.update(house);
		tx.commit();
	}
	
	

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	

}
