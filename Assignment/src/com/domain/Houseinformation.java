package com.domain;

/**
 * Houseinformation entity. 
 * @author 
 */

public class Houseinformation implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String houseName;
	private String houseAddress;
	private String houseType;
	private Integer capacity;
	private boolean isAvailable;
	private float price;
	private String description;
	private Integer houseownerId;
	private String houseownerName;
	private Integer houseownerPhone;

	// Constructors

	/** default constructor */
	public Houseinformation() {
	}

	/** full constructor */
	public Houseinformation(String houseName, String houseAddress, String houseType, Integer capacity,
			boolean isAvailable, float price, String description, Integer houseownerId, String houseownerName,
			Integer houseownerPhone) {
		this.houseName = houseName;
		this.houseAddress = houseAddress;
		this.houseType = houseType;
		this.capacity = capacity;
		this.isAvailable = isAvailable;
		this.price = price;
		this.description = description;
		this.houseownerId = houseownerId;
		this.houseownerName = houseownerName;
		this.houseownerPhone = houseownerPhone;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getHouseName() {
		return this.houseName;
	}

	public void setHouseName(String houseName) {
		this.houseName = houseName;
	}

	public String getHouseAddress() {
		return this.houseAddress;
	}

	public void setHouseAddress(String houseAddress) {
		this.houseAddress = houseAddress;
	}

	public String getHouseType() {
		return this.houseType;
	}

	public void setHouseType(String houseType) {
		this.houseType = houseType;
	}

	public Integer getCapacity() {
		return this.capacity;
	}

	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}

	public boolean getIsAvailable() {
		return this.isAvailable;
	}

	public void setIsAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}

	public float getPrice() {
		return this.price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getHouseownerId() {
		return this.houseownerId;
	}

	public void setHouseownerId(Integer houseownerId) {
		this.houseownerId = houseownerId;
	}

	public String getHouseownerName() {
		return this.houseownerName;
	}

	public void setHouseownerName(String houseownerName) {
		this.houseownerName = houseownerName;
	}

	public Integer getHouseownerPhone() {
		return this.houseownerPhone;
	}

	public void setHouseownerPhone(Integer houseownerPhone) {
		this.houseownerPhone = houseownerPhone;
	}

}