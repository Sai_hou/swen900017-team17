package com.action;

import java.util.List;

import com.domain.Houseinformation;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ModelDriven;
import com.service.HouseService;

public class HouseActionSet implements ModelDriven<Houseinformation>{
	
	private Houseinformation house;
	
	private HouseService houseService;
	
	private List<Houseinformation> houses;

	public Houseinformation getHouse() {
		return house;
	}

	public void setHouse(Houseinformation house) {
		this.house = house;
	}

	public HouseService getHouseService() {
		return houseService;
	}

	public void setHouseService(HouseService houseService) {
		this.houseService = houseService;
	}

	public void AddHouse(){
		houseService.addHouse(house);
		
	}
	
	public String searchHouse(){
		System.out.println("search");
		houses = houseService.allHouse();
		ActionContext.getContext().put("list", houses);
		System.out.println(houses.size());
		return "show";
		
	}
	
	public String deleteHouse(){
		System.out.println(house.getId());
		houseService.deleteHouse(house);
		return "deleteSuccess";
		
	}
	
	public String updateHouseRequest(){
		//find house through id
		System.out.println("coming in updateHouseRequest");
		house = houseService.findHouseById(house.getId());
		ActionContext.getContext().put("house", house);
		return "findSuccess";
	}
	
	public String updateHouse(){
		//find house through id
		System.out.println("coming in updateHouse");
		houseService.updateHouse(house);
		return "updateSuccess";
	}

	@Override
	public Houseinformation getModel() {
		// TODO Auto-generated method stub
		return new Houseinformation();
	}

}
