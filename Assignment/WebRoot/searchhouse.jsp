<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'index.jsp' starting page</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
</head>
<script type="text/javascript">

	function updateHouse(id) {
		
	window.location=" <%=request.getContextPath()%>/updateHouseRequest.action?house.id="+id;
	}
	
	function deleteHouse(id) {
	alert(id);
	  window.location=" <%=request.getContextPath()%>/deleteHouse.action?house.id="+id;
	}
</script>

<body>
	search houses
	</hr>
	<table align="center" border="1px" cellpadding="5">
		<tr>
			<td>HouseName
			</td>
			<td>HouseAddress
			</td>
			<td>HouseType
			</td>
			<td>Capacity
			</td>
			<td>Price
			</td>
			<td>Description
			</td>
			<td>update</td>
			<td>delete</td>
		</tr>
		 <s:iterator value ="list" var="house"> 
		<tr>
			<td><s:property value="houseName"/>
			</td>
			<td>
			<s:property value="houseAddress"/>
			</td>
			<td>
			<s:property value="houseType"/>
			</td>
			<td>
			<s:property value="capacity"/>
			</td>
			<td>
			<s:property value="price"/>
			</td>
			<td>
			<s:property value="description"/>
			</td>
			<td>
			<input type="button" name="${id}" value="update" onclick="updateHouse(${id})">
			</td>
			<td>
			<input type="button" name="${id}" value="delete" onclick="deleteHouse(${id})">
			</td>
		</tr>
		</s:iterator>
		
	</table>


</body>
</html>
