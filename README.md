# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is the project for SWEN90007. The system is based on JSP, and the database is mysql.
The system has four main features: adding, deleting, viewing and editing house.

### How do I get set up? ###

This is a Java web application using the following technologies: Backend: Struts2.1, Spring3.0.5 and Hibernate3.3.2 Framework 
FrontEnd: JavaScript,Jsp

After importing the project in you IDE, the first thing you need to do is add JARS in the project for SSH framework,
because we used the JARS which the MyEclipse offered, these JARS are not included in our project. Sorry about inconvinience.

The MySQL database is used in the project. When you need to test the project, you need to modify the dataSource 
configuration in applicationContext.xml such as driverClassName,url,username and password for connecting the database.

The table name is houseinformation and the houseinformation.sql script have been zipped in the file.
You need to import the sql script in the database management tool and execute it.
